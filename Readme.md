# Challenge 2

When using docker, you can start the jupyter environment simply via docker-compose up. Otherwise simply use your own (virtual) python env and install the required libraries in the requirements.txt.