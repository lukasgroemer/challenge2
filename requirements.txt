Pillow>=7.0.0,<8.0.0
matplotlib>=3.4.1,<3.5
pandas>=1.2.4,<1.3
seaborn>=0.11.1,<0.12
jupyter>=1.0.0,<1.1
Cython>=0.29.23,<0.30