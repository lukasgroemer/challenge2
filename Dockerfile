FROM python:3.9-slim

RUN apt-get update && apt-get install -y \
    	zlib1g-dev libjpeg-dev gcc musl-dev libfreetype6-dev && \
    apt-get clean

# jupyter notebook
EXPOSE 8888

ENTRYPOINT ["jupyter", "notebook", "--ip=0.0.0.0", "--no-browser", "--allow-root", "--notebook-dir=/workspace"]

COPY ./requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt

