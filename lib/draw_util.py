import numpy as np
from PIL import Image, ImageFont, ImageDraw, ImageEnhance

def draw_dot(img, xy, color='blue', radius=5):
    pil_img = Image.fromarray(np.uint8(img))
    draw = ImageDraw.Draw(pil_img)
    draw.ellipse((xy[0]-radius, xy[1]-radius, xy[0]+radius, xy[1]+radius), fill = color, outline = color)
    return np.array(pil_img)

def draw_dots(img, xys, color='blue', radius=5):
    pil_img = Image.fromarray(np.uint8(img))
    draw = ImageDraw.Draw(pil_img)
    for xy in xys:
        draw.ellipse((xy[0]-radius, xy[1]-radius, xy[0]+radius, xy[1]+radius), fill = color, outline = color)
    return np.array(pil_img)

def draw_line(img, xy1, xy2, color='blue', width=3):
    if (xy1 is None or xy2 is None):
        return img

    pil_img = Image.fromarray(np.uint8(img))
    draw = ImageDraw.Draw(pil_img)
    draw.line((xy1[0],xy1[1], xy2[0],xy2[1]), fill=color, width=width)

    return np.array(pil_img)

def draw_lines(img, lines, color='blue', width=3):
    for x1,y1,x2,y2 in lines:
        img = draw_line(img, (x1,y1), (x2,y2), color=color, width=width)
    return img

def draw_player(img, xy, number, color='blue', text_color="white", radius=20):
    font = ImageFont.truetype("fonts/Arial.ttf", radius)

    pil_img = Image.fromarray(np.uint8(img))
    draw = ImageDraw.Draw(pil_img)
    draw.ellipse((xy[0]-radius, xy[1]-radius, xy[0]+radius, xy[1]+radius), fill = color, outline = color)
    draw.text((xy[0] - (radius / 2), xy[1] - (radius / 2)), number,text_color,font=font)
    return np.array(pil_img)

def draw_plain_football_field(pitch_width, pitch_height, scale=10):
    color = np.array([111, 184, 101])
    field = np.zeros([pitch_height * scale, pitch_width * scale, 3], dtype=np.uint8)
    field[:,:,:] = color

    goal = 7.32
    goal_center = pitch_height / 2
    lines = [
        [0, 0, pitch_width, 0],
        [pitch_width, 0, pitch_width, pitch_height],
        [pitch_width, pitch_height, 0, pitch_height],
        [0, pitch_height, 0, 0],
        [pitch_width/2, 0, pitch_width/2, pitch_height],
        #16m left
        [0, goal_center - goal / 2 - 16, 16, goal_center - goal / 2 - 16],
        [16, goal_center - goal / 2 - 16, 16, goal_center + goal / 2 + 16],
        [16, goal_center + goal / 2 + 16, 0, goal_center + goal / 2 + 16],

        #16m right
        [pitch_width, goal_center - goal / 2 - 16, pitch_width - 16, goal_center - goal / 2 - 16],
        [pitch_width - 16, goal_center - goal / 2 - 16, pitch_width - 16, goal_center + goal / 2 + 16],
        [pitch_width - 16, goal_center + goal / 2 + 16, pitch_width, goal_center + goal / 2 + 16],

        #5m left
        [0, goal_center - goal / 2 - 5, 5, goal_center - goal / 2 - 5],
        [5, goal_center - goal / 2 - 5, 5, goal_center + goal / 2 + 5],
        [5, goal_center + goal / 2 + 5, 0, goal_center + goal / 2 + 5],

        #5m right
        [pitch_width, goal_center - goal / 2 - 5, pitch_width - 5, goal_center - goal / 2 - 5],
        [pitch_width - 5, goal_center - goal / 2 - 5, pitch_width - 5, goal_center + goal / 2 + 5],
        [pitch_width - 5, goal_center + goal / 2 + 5, pitch_width, goal_center + goal / 2 + 5]
    ]


    field = draw_dot(field, np.array([pitch_width/2, pitch_height/2]) * scale, radius=9.25*scale, color='white')
    field = draw_dot(field, np.array([pitch_width/2, pitch_height/2]) * scale, radius=8.75*scale, color=tuple(color))


    field = draw_lines(field, np.array(lines).astype(int) * scale, color='white', width=5)

    return field


def draw_track_and_relative_others(name, track, all_tracks, scale=10):
    img = draw_plain_football_field(105, 68, scale)

    for pos in [p[-1][1:] for p in all_tracks]:
        img = draw_player(img, np.array(pos) * scale, "?", radius = 25, color=(0, 255, 0))

    points = np.array([[p[1], p[2]] for p in track])

    img = draw_dots(img, points * scale, radius=1) # draw the trajectory of the first 40s (10 per second)

    return draw_player(img, points[-1] * scale, name, radius = 30, color=(0, 0, 0))
